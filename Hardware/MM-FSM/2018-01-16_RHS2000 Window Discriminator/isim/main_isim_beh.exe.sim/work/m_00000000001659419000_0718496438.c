/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/Max Murphy/Desktop/Preliminary intan/2018-01-16_RHS2000 Window Discriminator/RHS2000InterfaceXEM6010_release_170328/main.v";
static unsigned int ng1[] = {40U, 0U};
static unsigned int ng2[] = {42U, 0U};
static unsigned int ng3[] = {44U, 0U};
static unsigned int ng4[] = {10U, 0U};
static unsigned int ng5[] = {12U, 0U};
static unsigned int ng6[] = {46U, 0U};
static unsigned int ng7[] = {48U, 0U};
static unsigned int ng8[] = {3237937152U, 0U};
static unsigned int ng9[] = {0U, 0U};
static unsigned int ng10[] = {2U, 0U};
static unsigned int ng11[] = {3U, 0U};
static int ng12[] = {0, 0};
static int ng13[] = {1, 0};
static int ng14[] = {2, 0};
static int ng15[] = {3, 0};
static int ng16[] = {4, 0};
static int ng17[] = {5, 0};
static int ng18[] = {6, 0};
static int ng19[] = {7, 0};
static int ng20[] = {8, 0};
static int ng21[] = {9, 0};
static int ng22[] = {10, 0};
static int ng23[] = {11, 0};
static int ng24[] = {12, 0};
static int ng25[] = {13, 0};
static int ng26[] = {14, 0};
static int ng27[] = {15, 0};
static int ng28[] = {16, 0};
static int ng29[] = {17, 0};
static int ng30[] = {18, 0};
static int ng31[] = {19, 0};



static void Cont_4160_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    unsigned int t8;
    unsigned int t9;
    char *t10;
    unsigned int t11;
    unsigned int t12;
    char *t13;
    unsigned int t14;
    unsigned int t15;

LAB0:    t1 = (t0 + 6368U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(4160, ng0);
    t2 = ((char*)((ng1)));
    t3 = (t0 + 9888);
    t4 = (t3 + 56U);
    t5 = *((char **)t4);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    memset(t7, 0, 8);
    t8 = 255U;
    t9 = t8;
    t10 = (t2 + 4);
    t11 = *((unsigned int *)t2);
    t8 = (t8 & t11);
    t12 = *((unsigned int *)t10);
    t9 = (t9 & t12);
    t13 = (t7 + 4);
    t14 = *((unsigned int *)t7);
    *((unsigned int *)t7) = (t14 | t8);
    t15 = *((unsigned int *)t13);
    *((unsigned int *)t13) = (t15 | t9);
    xsi_driver_vfirst_trans(t3, 0, 7);

LAB1:    return;
}

static void Cont_4161_1(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    unsigned int t8;
    unsigned int t9;
    char *t10;
    unsigned int t11;
    unsigned int t12;
    char *t13;
    unsigned int t14;
    unsigned int t15;

LAB0:    t1 = (t0 + 6616U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(4161, ng0);
    t2 = ((char*)((ng2)));
    t3 = (t0 + 9952);
    t4 = (t3 + 56U);
    t5 = *((char **)t4);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    memset(t7, 0, 8);
    t8 = 255U;
    t9 = t8;
    t10 = (t2 + 4);
    t11 = *((unsigned int *)t2);
    t8 = (t8 & t11);
    t12 = *((unsigned int *)t10);
    t9 = (t9 & t12);
    t13 = (t7 + 4);
    t14 = *((unsigned int *)t7);
    *((unsigned int *)t7) = (t14 | t8);
    t15 = *((unsigned int *)t13);
    *((unsigned int *)t13) = (t15 | t9);
    xsi_driver_vfirst_trans(t3, 0, 7);

LAB1:    return;
}

static void Cont_4162_2(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    unsigned int t8;
    unsigned int t9;
    char *t10;
    unsigned int t11;
    unsigned int t12;
    char *t13;
    unsigned int t14;
    unsigned int t15;

LAB0:    t1 = (t0 + 6864U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(4162, ng0);
    t2 = ((char*)((ng3)));
    t3 = (t0 + 10016);
    t4 = (t3 + 56U);
    t5 = *((char **)t4);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    memset(t7, 0, 8);
    t8 = 255U;
    t9 = t8;
    t10 = (t2 + 4);
    t11 = *((unsigned int *)t2);
    t8 = (t8 & t11);
    t12 = *((unsigned int *)t10);
    t9 = (t9 & t12);
    t13 = (t7 + 4);
    t14 = *((unsigned int *)t7);
    *((unsigned int *)t7) = (t14 | t8);
    t15 = *((unsigned int *)t13);
    *((unsigned int *)t13) = (t15 | t9);
    xsi_driver_vfirst_trans(t3, 0, 7);

LAB1:    return;
}

static void Cont_4166_3(char *t0)
{
    char t3[8];
    char t4[8];
    char *t1;
    char *t2;
    char *t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    char *t11;
    char *t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    char *t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    unsigned int t27;
    unsigned int t28;
    char *t29;
    unsigned int t30;
    unsigned int t31;
    char *t32;
    unsigned int t33;
    unsigned int t34;
    char *t35;

LAB0:    t1 = (t0 + 7112U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(4166, ng0);
    t2 = (t0 + 1528U);
    t5 = *((char **)t2);
    memset(t4, 0, 8);
    t2 = (t5 + 4);
    t6 = *((unsigned int *)t2);
    t7 = (~(t6));
    t8 = *((unsigned int *)t5);
    t9 = (t8 & t7);
    t10 = (t9 & 1U);
    if (t10 != 0)
        goto LAB4;

LAB5:    if (*((unsigned int *)t2) != 0)
        goto LAB6;

LAB7:    t12 = (t4 + 4);
    t13 = *((unsigned int *)t4);
    t14 = *((unsigned int *)t12);
    t15 = (t13 || t14);
    if (t15 > 0)
        goto LAB8;

LAB9:    t17 = *((unsigned int *)t4);
    t18 = (~(t17));
    t19 = *((unsigned int *)t12);
    t20 = (t18 || t19);
    if (t20 > 0)
        goto LAB10;

LAB11:    if (*((unsigned int *)t12) > 0)
        goto LAB12;

LAB13:    if (*((unsigned int *)t4) > 0)
        goto LAB14;

LAB15:    memcpy(t3, t21, 8);

LAB16:    t22 = (t0 + 10080);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    t25 = (t24 + 56U);
    t26 = *((char **)t25);
    memset(t26, 0, 8);
    t27 = 255U;
    t28 = t27;
    t29 = (t3 + 4);
    t30 = *((unsigned int *)t3);
    t27 = (t27 & t30);
    t31 = *((unsigned int *)t29);
    t28 = (t28 & t31);
    t32 = (t26 + 4);
    t33 = *((unsigned int *)t26);
    *((unsigned int *)t26) = (t33 | t27);
    t34 = *((unsigned int *)t32);
    *((unsigned int *)t32) = (t34 | t28);
    xsi_driver_vfirst_trans(t22, 0, 7);
    t35 = (t0 + 9664);
    *((int *)t35) = 1;

LAB1:    return;
LAB4:    *((unsigned int *)t4) = 1;
    goto LAB7;

LAB6:    t11 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t11) = 1;
    goto LAB7;

LAB8:    t16 = ((char*)((ng4)));
    goto LAB9;

LAB10:    t21 = ((char*)((ng5)));
    goto LAB11;

LAB12:    xsi_vlog_unsigned_bit_combine(t3, 8, t16, 8, t21, 8);
    goto LAB16;

LAB14:    memcpy(t3, t16, 8);
    goto LAB16;

}

static void Cont_4170_4(char *t0)
{
    char t3[8];
    char t4[8];
    char *t1;
    char *t2;
    char *t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    char *t11;
    char *t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    char *t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    unsigned int t27;
    unsigned int t28;
    char *t29;
    unsigned int t30;
    unsigned int t31;
    char *t32;
    unsigned int t33;
    unsigned int t34;
    char *t35;

LAB0:    t1 = (t0 + 7360U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(4170, ng0);
    t2 = (t0 + 1688U);
    t5 = *((char **)t2);
    memset(t4, 0, 8);
    t2 = (t5 + 4);
    t6 = *((unsigned int *)t2);
    t7 = (~(t6));
    t8 = *((unsigned int *)t5);
    t9 = (t8 & t7);
    t10 = (t9 & 1U);
    if (t10 != 0)
        goto LAB4;

LAB5:    if (*((unsigned int *)t2) != 0)
        goto LAB6;

LAB7:    t12 = (t4 + 4);
    t13 = *((unsigned int *)t4);
    t14 = *((unsigned int *)t12);
    t15 = (t13 || t14);
    if (t15 > 0)
        goto LAB8;

LAB9:    t17 = *((unsigned int *)t4);
    t18 = (~(t17));
    t19 = *((unsigned int *)t12);
    t20 = (t18 || t19);
    if (t20 > 0)
        goto LAB10;

LAB11:    if (*((unsigned int *)t12) > 0)
        goto LAB12;

LAB13:    if (*((unsigned int *)t4) > 0)
        goto LAB14;

LAB15:    memcpy(t3, t21, 8);

LAB16:    t22 = (t0 + 10144);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    t25 = (t24 + 56U);
    t26 = *((char **)t25);
    memset(t26, 0, 8);
    t27 = 255U;
    t28 = t27;
    t29 = (t3 + 4);
    t30 = *((unsigned int *)t3);
    t27 = (t27 & t30);
    t31 = *((unsigned int *)t29);
    t28 = (t28 & t31);
    t32 = (t26 + 4);
    t33 = *((unsigned int *)t26);
    *((unsigned int *)t26) = (t33 | t27);
    t34 = *((unsigned int *)t32);
    *((unsigned int *)t32) = (t34 | t28);
    xsi_driver_vfirst_trans(t22, 0, 7);
    t35 = (t0 + 9680);
    *((int *)t35) = 1;

LAB1:    return;
LAB4:    *((unsigned int *)t4) = 1;
    goto LAB7;

LAB6:    t11 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t11) = 1;
    goto LAB7;

LAB8:    t16 = ((char*)((ng6)));
    goto LAB9;

LAB10:    t21 = ((char*)((ng7)));
    goto LAB11;

LAB12:    xsi_vlog_unsigned_bit_combine(t3, 8, t16, 8, t21, 8);
    goto LAB16;

LAB14:    memcpy(t3, t16, 8);
    goto LAB16;

}

static void Cont_4173_5(char *t0)
{
    char t3[8];
    char t4[8];
    char *t1;
    char *t2;
    char *t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    char *t11;
    char *t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    char *t16;
    char *t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;

LAB0:    t1 = (t0 + 7608U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(4173, ng0);
    t2 = (t0 + 2008U);
    t5 = *((char **)t2);
    memset(t4, 0, 8);
    t2 = (t5 + 4);
    t6 = *((unsigned int *)t2);
    t7 = (~(t6));
    t8 = *((unsigned int *)t5);
    t9 = (t8 & t7);
    t10 = (t9 & 1U);
    if (t10 != 0)
        goto LAB4;

LAB5:    if (*((unsigned int *)t2) != 0)
        goto LAB6;

LAB7:    t12 = (t4 + 4);
    t13 = *((unsigned int *)t4);
    t14 = *((unsigned int *)t12);
    t15 = (t13 || t14);
    if (t15 > 0)
        goto LAB8;

LAB9:    t18 = *((unsigned int *)t4);
    t19 = (~(t18));
    t20 = *((unsigned int *)t12);
    t21 = (t19 || t20);
    if (t21 > 0)
        goto LAB10;

LAB11:    if (*((unsigned int *)t12) > 0)
        goto LAB12;

LAB13:    if (*((unsigned int *)t4) > 0)
        goto LAB14;

LAB15:    memcpy(t3, t16, 8);

LAB16:    t22 = (t0 + 10208);
    t23 = (t22 + 56U);
    t24 = *((char **)t23);
    t25 = (t24 + 56U);
    t26 = *((char **)t25);
    memcpy(t26, t3, 8);
    xsi_driver_vfirst_trans(t22, 0, 31);
    t27 = (t0 + 9696);
    *((int *)t27) = 1;

LAB1:    return;
LAB4:    *((unsigned int *)t4) = 1;
    goto LAB7;

LAB6:    t11 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t11) = 1;
    goto LAB7;

LAB8:    t16 = (t0 + 1848U);
    t17 = *((char **)t16);
    goto LAB9;

LAB10:    t16 = ((char*)((ng8)));
    goto LAB11;

LAB12:    xsi_vlog_unsigned_bit_combine(t3, 32, t17, 32, t16, 32);
    goto LAB16;

LAB14:    memcpy(t3, t17, 8);
    goto LAB16;

}

static void Cont_4177_6(char *t0)
{
    char t3[8];
    char t4[8];
    char *t1;
    char *t2;
    char *t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    char *t11;
    char *t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    char *t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    unsigned int t27;
    unsigned int t28;
    char *t29;
    unsigned int t30;
    unsigned int t31;
    char *t32;
    unsigned int t33;
    unsigned int t34;
    char *t35;

LAB0:    t1 = (t0 + 7856U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(4177, ng0);
    t2 = (t0 + 1208U);
    t5 = *((char **)t2);
    memset(t4, 0, 8);
    t2 = (t5 + 4);
    t6 = *((unsigned int *)t2);
    t7 = (~(t6));
    t8 = *((unsigned int *)t5);
    t9 = (t8 & t7);
    t10 = (t9 & 1U);
    if (t10 != 0)
        goto LAB4;

LAB5:    if (*((unsigned int *)t2) != 0)
        goto LAB6;

LAB7:    t12 = (t4 + 4);
    t13 = *((unsigned int *)t4);
    t14 = *((unsigned int *)t12);
    t15 = (t13 || t14);
    if (t15 > 0)
        goto LAB8;

LAB9:    t17 = *((unsigned int *)t4);
    t18 = (~(t17));
    t19 = *((unsigned int *)t12);
    t20 = (t18 || t19);
    if (t20 > 0)
        goto LAB10;

LAB11:    if (*((unsigned int *)t12) > 0)
        goto LAB12;

LAB13:    if (*((unsigned int *)t4) > 0)
        goto LAB14;

LAB15:    memcpy(t3, t22, 8);

LAB16:    t21 = (t0 + 10272);
    t23 = (t21 + 56U);
    t24 = *((char **)t23);
    t25 = (t24 + 56U);
    t26 = *((char **)t25);
    memset(t26, 0, 8);
    t27 = 65535U;
    t28 = t27;
    t29 = (t3 + 4);
    t30 = *((unsigned int *)t3);
    t27 = (t27 & t30);
    t31 = *((unsigned int *)t29);
    t28 = (t28 & t31);
    t32 = (t26 + 4);
    t33 = *((unsigned int *)t26);
    *((unsigned int *)t26) = (t33 | t27);
    t34 = *((unsigned int *)t32);
    *((unsigned int *)t32) = (t34 | t28);
    xsi_driver_vfirst_trans(t21, 0, 15);
    t35 = (t0 + 9712);
    *((int *)t35) = 1;

LAB1:    return;
LAB4:    *((unsigned int *)t4) = 1;
    goto LAB7;

LAB6:    t11 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t11) = 1;
    goto LAB7;

LAB8:    t16 = ((char*)((ng9)));
    goto LAB9;

LAB10:    t21 = (t0 + 2488U);
    t22 = *((char **)t21);
    goto LAB11;

LAB12:    xsi_vlog_unsigned_bit_combine(t3, 16, t16, 16, t22, 16);
    goto LAB16;

LAB14:    memcpy(t3, t16, 8);
    goto LAB16;

}

static void Cont_4178_7(char *t0)
{
    char t3[8];
    char t4[8];
    char *t1;
    char *t2;
    char *t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    char *t11;
    char *t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    char *t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    unsigned int t27;
    unsigned int t28;
    char *t29;
    unsigned int t30;
    unsigned int t31;
    char *t32;
    unsigned int t33;
    unsigned int t34;
    char *t35;

LAB0:    t1 = (t0 + 8104U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(4178, ng0);
    t2 = (t0 + 1208U);
    t5 = *((char **)t2);
    memset(t4, 0, 8);
    t2 = (t5 + 4);
    t6 = *((unsigned int *)t2);
    t7 = (~(t6));
    t8 = *((unsigned int *)t5);
    t9 = (t8 & t7);
    t10 = (t9 & 1U);
    if (t10 != 0)
        goto LAB4;

LAB5:    if (*((unsigned int *)t2) != 0)
        goto LAB6;

LAB7:    t12 = (t4 + 4);
    t13 = *((unsigned int *)t4);
    t14 = *((unsigned int *)t12);
    t15 = (t13 || t14);
    if (t15 > 0)
        goto LAB8;

LAB9:    t17 = *((unsigned int *)t4);
    t18 = (~(t17));
    t19 = *((unsigned int *)t12);
    t20 = (t18 || t19);
    if (t20 > 0)
        goto LAB10;

LAB11:    if (*((unsigned int *)t12) > 0)
        goto LAB12;

LAB13:    if (*((unsigned int *)t4) > 0)
        goto LAB14;

LAB15:    memcpy(t3, t22, 8);

LAB16:    t21 = (t0 + 10336);
    t23 = (t21 + 56U);
    t24 = *((char **)t23);
    t25 = (t24 + 56U);
    t26 = *((char **)t25);
    memset(t26, 0, 8);
    t27 = 65535U;
    t28 = t27;
    t29 = (t3 + 4);
    t30 = *((unsigned int *)t3);
    t27 = (t27 & t30);
    t31 = *((unsigned int *)t29);
    t28 = (t28 & t31);
    t32 = (t26 + 4);
    t33 = *((unsigned int *)t26);
    *((unsigned int *)t26) = (t33 | t27);
    t34 = *((unsigned int *)t32);
    *((unsigned int *)t32) = (t34 | t28);
    xsi_driver_vfirst_trans(t21, 0, 15);
    t35 = (t0 + 9728);
    *((int *)t35) = 1;

LAB1:    return;
LAB4:    *((unsigned int *)t4) = 1;
    goto LAB7;

LAB6:    t11 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t11) = 1;
    goto LAB7;

LAB8:    t16 = ((char*)((ng9)));
    goto LAB9;

LAB10:    t21 = (t0 + 2648U);
    t22 = *((char **)t21);
    goto LAB11;

LAB12:    xsi_vlog_unsigned_bit_combine(t3, 16, t16, 16, t22, 16);
    goto LAB16;

LAB14:    memcpy(t3, t16, 8);
    goto LAB16;

}

static void Cont_4179_8(char *t0)
{
    char t3[8];
    char t4[8];
    char *t1;
    char *t2;
    char *t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    char *t11;
    char *t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    char *t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    unsigned int t27;
    unsigned int t28;
    char *t29;
    unsigned int t30;
    unsigned int t31;
    char *t32;
    unsigned int t33;
    unsigned int t34;
    char *t35;

LAB0:    t1 = (t0 + 8352U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(4179, ng0);
    t2 = (t0 + 1208U);
    t5 = *((char **)t2);
    memset(t4, 0, 8);
    t2 = (t5 + 4);
    t6 = *((unsigned int *)t2);
    t7 = (~(t6));
    t8 = *((unsigned int *)t5);
    t9 = (t8 & t7);
    t10 = (t9 & 1U);
    if (t10 != 0)
        goto LAB4;

LAB5:    if (*((unsigned int *)t2) != 0)
        goto LAB6;

LAB7:    t12 = (t4 + 4);
    t13 = *((unsigned int *)t4);
    t14 = *((unsigned int *)t12);
    t15 = (t13 || t14);
    if (t15 > 0)
        goto LAB8;

LAB9:    t17 = *((unsigned int *)t4);
    t18 = (~(t17));
    t19 = *((unsigned int *)t12);
    t20 = (t18 || t19);
    if (t20 > 0)
        goto LAB10;

LAB11:    if (*((unsigned int *)t12) > 0)
        goto LAB12;

LAB13:    if (*((unsigned int *)t4) > 0)
        goto LAB14;

LAB15:    memcpy(t3, t22, 8);

LAB16:    t21 = (t0 + 10400);
    t23 = (t21 + 56U);
    t24 = *((char **)t23);
    t25 = (t24 + 56U);
    t26 = *((char **)t25);
    memset(t26, 0, 8);
    t27 = 65535U;
    t28 = t27;
    t29 = (t3 + 4);
    t30 = *((unsigned int *)t3);
    t27 = (t27 & t30);
    t31 = *((unsigned int *)t29);
    t28 = (t28 & t31);
    t32 = (t26 + 4);
    t33 = *((unsigned int *)t26);
    *((unsigned int *)t26) = (t33 | t27);
    t34 = *((unsigned int *)t32);
    *((unsigned int *)t32) = (t34 | t28);
    xsi_driver_vfirst_trans(t21, 0, 15);
    t35 = (t0 + 9744);
    *((int *)t35) = 1;

LAB1:    return;
LAB4:    *((unsigned int *)t4) = 1;
    goto LAB7;

LAB6:    t11 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t11) = 1;
    goto LAB7;

LAB8:    t16 = ((char*)((ng9)));
    goto LAB9;

LAB10:    t21 = (t0 + 2808U);
    t22 = *((char **)t21);
    goto LAB11;

LAB12:    xsi_vlog_unsigned_bit_combine(t3, 16, t16, 16, t22, 16);
    goto LAB16;

LAB14:    memcpy(t3, t16, 8);
    goto LAB16;

}

static void Cont_4180_9(char *t0)
{
    char t3[8];
    char t4[8];
    char *t1;
    char *t2;
    char *t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    char *t11;
    char *t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    char *t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    unsigned int t27;
    unsigned int t28;
    char *t29;
    unsigned int t30;
    unsigned int t31;
    char *t32;
    unsigned int t33;
    unsigned int t34;
    char *t35;

LAB0:    t1 = (t0 + 8600U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(4180, ng0);
    t2 = (t0 + 1208U);
    t5 = *((char **)t2);
    memset(t4, 0, 8);
    t2 = (t5 + 4);
    t6 = *((unsigned int *)t2);
    t7 = (~(t6));
    t8 = *((unsigned int *)t5);
    t9 = (t8 & t7);
    t10 = (t9 & 1U);
    if (t10 != 0)
        goto LAB4;

LAB5:    if (*((unsigned int *)t2) != 0)
        goto LAB6;

LAB7:    t12 = (t4 + 4);
    t13 = *((unsigned int *)t4);
    t14 = *((unsigned int *)t12);
    t15 = (t13 || t14);
    if (t15 > 0)
        goto LAB8;

LAB9:    t17 = *((unsigned int *)t4);
    t18 = (~(t17));
    t19 = *((unsigned int *)t12);
    t20 = (t18 || t19);
    if (t20 > 0)
        goto LAB10;

LAB11:    if (*((unsigned int *)t12) > 0)
        goto LAB12;

LAB13:    if (*((unsigned int *)t4) > 0)
        goto LAB14;

LAB15:    memcpy(t3, t22, 8);

LAB16:    t21 = (t0 + 10464);
    t23 = (t21 + 56U);
    t24 = *((char **)t23);
    t25 = (t24 + 56U);
    t26 = *((char **)t25);
    memset(t26, 0, 8);
    t27 = 65535U;
    t28 = t27;
    t29 = (t3 + 4);
    t30 = *((unsigned int *)t3);
    t27 = (t27 & t30);
    t31 = *((unsigned int *)t29);
    t28 = (t28 & t31);
    t32 = (t26 + 4);
    t33 = *((unsigned int *)t26);
    *((unsigned int *)t26) = (t33 | t27);
    t34 = *((unsigned int *)t32);
    *((unsigned int *)t32) = (t34 | t28);
    xsi_driver_vfirst_trans(t21, 0, 15);
    t35 = (t0 + 9760);
    *((int *)t35) = 1;

LAB1:    return;
LAB4:    *((unsigned int *)t4) = 1;
    goto LAB7;

LAB6:    t11 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t11) = 1;
    goto LAB7;

LAB8:    t16 = ((char*)((ng9)));
    goto LAB9;

LAB10:    t21 = (t0 + 2968U);
    t22 = *((char **)t21);
    goto LAB11;

LAB12:    xsi_vlog_unsigned_bit_combine(t3, 16, t16, 16, t22, 16);
    goto LAB16;

LAB14:    memcpy(t3, t16, 8);
    goto LAB16;

}

static void Cont_4185_10(char *t0)
{
    char t3[8];
    char t4[8];
    char t16[8];
    char t26[8];
    char *t1;
    char *t2;
    char *t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    char *t11;
    char *t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    char *t17;
    char *t18;
    char *t19;
    char *t20;
    char *t21;
    unsigned int t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t25;
    char *t27;
    char *t28;
    char *t29;
    char *t30;
    char *t31;
    char *t32;
    char *t33;
    char *t34;
    char *t35;
    char *t36;
    char *t37;

LAB0:    t1 = (t0 + 8848U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(4185, ng0);
    t2 = (t0 + 3128U);
    t5 = *((char **)t2);
    memset(t4, 0, 8);
    t2 = (t5 + 4);
    t6 = *((unsigned int *)t2);
    t7 = (~(t6));
    t8 = *((unsigned int *)t5);
    t9 = (t8 & t7);
    t10 = (t9 & 1U);
    if (t10 != 0)
        goto LAB4;

LAB5:    if (*((unsigned int *)t2) != 0)
        goto LAB6;

LAB7:    t12 = (t4 + 4);
    t13 = *((unsigned int *)t4);
    t14 = *((unsigned int *)t12);
    t15 = (t13 || t14);
    if (t15 > 0)
        goto LAB8;

LAB9:    t22 = *((unsigned int *)t4);
    t23 = (~(t22));
    t24 = *((unsigned int *)t12);
    t25 = (t23 || t24);
    if (t25 > 0)
        goto LAB10;

LAB11:    if (*((unsigned int *)t12) > 0)
        goto LAB12;

LAB13:    if (*((unsigned int *)t4) > 0)
        goto LAB14;

LAB15:    memcpy(t3, t26, 8);

LAB16:    t32 = (t0 + 10528);
    t33 = (t32 + 56U);
    t34 = *((char **)t33);
    t35 = (t34 + 56U);
    t36 = *((char **)t35);
    memcpy(t36, t3, 8);
    xsi_driver_vfirst_trans(t32, 0, 31);
    t37 = (t0 + 9776);
    *((int *)t37) = 1;

LAB1:    return;
LAB4:    *((unsigned int *)t4) = 1;
    goto LAB7;

LAB6:    t11 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t11) = 1;
    goto LAB7;

LAB8:    t17 = (t0 + 4728U);
    t18 = *((char **)t17);
    t17 = (t0 + 3288U);
    t19 = *((char **)t17);
    t17 = ((char*)((ng9)));
    t20 = ((char*)((ng9)));
    t21 = ((char*)((ng10)));
    xsi_vlogtype_concat(t16, 32, 32, 5U, t21, 2, t20, 2, t17, 4, t19, 8, t18, 16);
    goto LAB9;

LAB10:    t27 = ((char*)((ng9)));
    t28 = (t0 + 3608U);
    t29 = *((char **)t28);
    t28 = ((char*)((ng9)));
    t30 = ((char*)((ng9)));
    t31 = ((char*)((ng11)));
    xsi_vlogtype_concat(t26, 32, 32, 5U, t31, 2, t30, 2, t28, 4, t29, 8, t27, 16);
    goto LAB11;

LAB12:    xsi_vlog_unsigned_bit_combine(t3, 32, t16, 32, t26, 32);
    goto LAB16;

LAB14:    memcpy(t3, t16, 8);
    goto LAB16;

}

static void Cont_4190_11(char *t0)
{
    char t3[8];
    char t4[8];
    char t16[8];
    char t26[8];
    char *t1;
    char *t2;
    char *t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    char *t11;
    char *t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    char *t17;
    char *t18;
    char *t19;
    char *t20;
    char *t21;
    unsigned int t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t25;
    char *t27;
    char *t28;
    char *t29;
    char *t30;
    char *t31;
    char *t32;
    char *t33;
    char *t34;
    char *t35;
    char *t36;
    char *t37;

LAB0:    t1 = (t0 + 9096U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(4190, ng0);
    t2 = (t0 + 3128U);
    t5 = *((char **)t2);
    memset(t4, 0, 8);
    t2 = (t5 + 4);
    t6 = *((unsigned int *)t2);
    t7 = (~(t6));
    t8 = *((unsigned int *)t5);
    t9 = (t8 & t7);
    t10 = (t9 & 1U);
    if (t10 != 0)
        goto LAB4;

LAB5:    if (*((unsigned int *)t2) != 0)
        goto LAB6;

LAB7:    t12 = (t4 + 4);
    t13 = *((unsigned int *)t4);
    t14 = *((unsigned int *)t12);
    t15 = (t13 || t14);
    if (t15 > 0)
        goto LAB8;

LAB9:    t22 = *((unsigned int *)t4);
    t23 = (~(t22));
    t24 = *((unsigned int *)t12);
    t25 = (t23 || t24);
    if (t25 > 0)
        goto LAB10;

LAB11:    if (*((unsigned int *)t12) > 0)
        goto LAB12;

LAB13:    if (*((unsigned int *)t4) > 0)
        goto LAB14;

LAB15:    memcpy(t3, t26, 8);

LAB16:    t32 = (t0 + 10592);
    t33 = (t32 + 56U);
    t34 = *((char **)t33);
    t35 = (t34 + 56U);
    t36 = *((char **)t35);
    memcpy(t36, t3, 8);
    xsi_driver_vfirst_trans(t32, 0, 31);
    t37 = (t0 + 9792);
    *((int *)t37) = 1;

LAB1:    return;
LAB4:    *((unsigned int *)t4) = 1;
    goto LAB7;

LAB6:    t11 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t11) = 1;
    goto LAB7;

LAB8:    t17 = (t0 + 4568U);
    t18 = *((char **)t17);
    t17 = (t0 + 3448U);
    t19 = *((char **)t17);
    t17 = ((char*)((ng9)));
    t20 = ((char*)((ng10)));
    t21 = ((char*)((ng10)));
    xsi_vlogtype_concat(t16, 32, 32, 5U, t21, 2, t20, 2, t17, 4, t19, 8, t18, 16);
    goto LAB9;

LAB10:    t27 = (t0 + 4568U);
    t28 = *((char **)t27);
    t27 = (t0 + 3448U);
    t29 = *((char **)t27);
    t27 = ((char*)((ng9)));
    t30 = ((char*)((ng11)));
    t31 = ((char*)((ng10)));
    xsi_vlogtype_concat(t26, 32, 32, 5U, t31, 2, t30, 2, t27, 4, t29, 8, t28, 16);
    goto LAB11;

LAB12:    xsi_vlog_unsigned_bit_combine(t3, 32, t16, 32, t26, 32);
    goto LAB16;

LAB14:    memcpy(t3, t16, 8);
    goto LAB16;

}

static void Always_4193_12(char *t0)
{
    char t7[8];
    char t16[8];
    char t25[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    int t6;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t26;
    unsigned int t27;
    unsigned int t28;
    unsigned int t29;
    char *t30;

LAB0:    t1 = (t0 + 9344U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(4193, ng0);
    t2 = (t0 + 9808);
    *((int *)t2) = 1;
    t3 = (t0 + 9376);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(4193, ng0);

LAB5:    xsi_set_current_line(4194, ng0);
    t4 = (t0 + 1048U);
    t5 = *((char **)t4);

LAB6:    t4 = ((char*)((ng12)));
    t6 = xsi_vlog_unsigned_case_compare(t5, 6, t4, 32);
    if (t6 == 1)
        goto LAB7;

LAB8:    t2 = ((char*)((ng13)));
    t6 = xsi_vlog_unsigned_case_compare(t5, 6, t2, 32);
    if (t6 == 1)
        goto LAB9;

LAB10:    t2 = ((char*)((ng14)));
    t6 = xsi_vlog_unsigned_case_compare(t5, 6, t2, 32);
    if (t6 == 1)
        goto LAB11;

LAB12:    t2 = ((char*)((ng15)));
    t6 = xsi_vlog_unsigned_case_compare(t5, 6, t2, 32);
    if (t6 == 1)
        goto LAB13;

LAB14:    t2 = ((char*)((ng16)));
    t6 = xsi_vlog_unsigned_case_compare(t5, 6, t2, 32);
    if (t6 == 1)
        goto LAB15;

LAB16:    t2 = ((char*)((ng17)));
    t6 = xsi_vlog_unsigned_case_compare(t5, 6, t2, 32);
    if (t6 == 1)
        goto LAB17;

LAB18:    t2 = ((char*)((ng18)));
    t6 = xsi_vlog_unsigned_case_compare(t5, 6, t2, 32);
    if (t6 == 1)
        goto LAB19;

LAB20:    t2 = ((char*)((ng19)));
    t6 = xsi_vlog_unsigned_case_compare(t5, 6, t2, 32);
    if (t6 == 1)
        goto LAB21;

LAB22:    t2 = ((char*)((ng20)));
    t6 = xsi_vlog_unsigned_case_compare(t5, 6, t2, 32);
    if (t6 == 1)
        goto LAB23;

LAB24:    t2 = ((char*)((ng21)));
    t6 = xsi_vlog_unsigned_case_compare(t5, 6, t2, 32);
    if (t6 == 1)
        goto LAB25;

LAB26:    t2 = ((char*)((ng22)));
    t6 = xsi_vlog_unsigned_case_compare(t5, 6, t2, 32);
    if (t6 == 1)
        goto LAB27;

LAB28:    t2 = ((char*)((ng23)));
    t6 = xsi_vlog_unsigned_case_compare(t5, 6, t2, 32);
    if (t6 == 1)
        goto LAB29;

LAB30:    t2 = ((char*)((ng24)));
    t6 = xsi_vlog_unsigned_case_compare(t5, 6, t2, 32);
    if (t6 == 1)
        goto LAB31;

LAB32:    t2 = ((char*)((ng25)));
    t6 = xsi_vlog_unsigned_case_compare(t5, 6, t2, 32);
    if (t6 == 1)
        goto LAB33;

LAB34:    t2 = ((char*)((ng26)));
    t6 = xsi_vlog_unsigned_case_compare(t5, 6, t2, 32);
    if (t6 == 1)
        goto LAB35;

LAB36:    t2 = ((char*)((ng27)));
    t6 = xsi_vlog_unsigned_case_compare(t5, 6, t2, 32);
    if (t6 == 1)
        goto LAB37;

LAB38:    t2 = ((char*)((ng28)));
    t6 = xsi_vlog_unsigned_case_compare(t5, 6, t2, 32);
    if (t6 == 1)
        goto LAB39;

LAB40:    t2 = ((char*)((ng29)));
    t6 = xsi_vlog_unsigned_case_compare(t5, 6, t2, 32);
    if (t6 == 1)
        goto LAB41;

LAB42:    t2 = ((char*)((ng30)));
    t6 = xsi_vlog_unsigned_case_compare(t5, 6, t2, 32);
    if (t6 == 1)
        goto LAB43;

LAB44:    t2 = ((char*)((ng31)));
    t6 = xsi_vlog_unsigned_case_compare(t5, 6, t2, 32);
    if (t6 == 1)
        goto LAB45;

LAB46:
LAB48:
LAB47:    xsi_set_current_line(4215, ng0);
    t2 = ((char*)((ng9)));
    t3 = (t0 + 5448);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 32, 0LL);

LAB49:    goto LAB2;

LAB7:    xsi_set_current_line(4195, ng0);
    t8 = ((char*)((ng9)));
    t9 = (t0 + 1048U);
    t10 = *((char **)t9);
    t9 = ((char*)((ng9)));
    t11 = (t0 + 1368U);
    t12 = *((char **)t11);
    t11 = (t0 + 2168U);
    t13 = *((char **)t11);
    t11 = ((char*)((ng9)));
    t14 = ((char*)((ng9)));
    xsi_vlogtype_concat(t7, 32, 32, 7U, t14, 2, t11, 2, t13, 1, t12, 1, t9, 4, t10, 6, t8, 16);
    t15 = (t0 + 5448);
    xsi_vlogvar_wait_assign_value(t15, t7, 0, 0, 32, 0LL);
    goto LAB49;

LAB9:    xsi_set_current_line(4196, ng0);
    t3 = ((char*)((ng9)));
    t4 = (t0 + 1048U);
    t8 = *((char **)t4);
    t4 = ((char*)((ng9)));
    t9 = (t0 + 1368U);
    t10 = *((char **)t9);
    t9 = (t0 + 2168U);
    t11 = *((char **)t9);
    t9 = ((char*)((ng9)));
    t12 = ((char*)((ng9)));
    xsi_vlogtype_concat(t7, 32, 32, 7U, t12, 2, t9, 2, t11, 1, t10, 1, t4, 4, t8, 6, t3, 16);
    t13 = (t0 + 5448);
    xsi_vlogvar_wait_assign_value(t13, t7, 0, 0, 32, 0LL);
    goto LAB49;

LAB11:    xsi_set_current_line(4197, ng0);
    t3 = ((char*)((ng9)));
    t4 = (t0 + 1048U);
    t8 = *((char **)t4);
    t4 = ((char*)((ng9)));
    t9 = (t0 + 1368U);
    t10 = *((char **)t9);
    t9 = (t0 + 2168U);
    t11 = *((char **)t9);
    t9 = ((char*)((ng9)));
    t12 = ((char*)((ng9)));
    xsi_vlogtype_concat(t7, 32, 32, 7U, t12, 2, t9, 2, t11, 1, t10, 1, t4, 4, t8, 6, t3, 16);
    t13 = (t0 + 5448);
    xsi_vlogvar_wait_assign_value(t13, t7, 0, 0, 32, 0LL);
    goto LAB49;

LAB13:    xsi_set_current_line(4198, ng0);
    t3 = ((char*)((ng9)));
    t4 = (t0 + 1048U);
    t8 = *((char **)t4);
    t4 = ((char*)((ng9)));
    t9 = (t0 + 1368U);
    t10 = *((char **)t9);
    t9 = (t0 + 2168U);
    t11 = *((char **)t9);
    t9 = ((char*)((ng9)));
    t12 = ((char*)((ng9)));
    xsi_vlogtype_concat(t7, 32, 32, 7U, t12, 2, t9, 2, t11, 1, t10, 1, t4, 4, t8, 6, t3, 16);
    t13 = (t0 + 5448);
    xsi_vlogvar_wait_assign_value(t13, t7, 0, 0, 32, 0LL);
    goto LAB49;

LAB15:    xsi_set_current_line(4199, ng0);
    t3 = ((char*)((ng9)));
    t4 = (t0 + 1048U);
    t8 = *((char **)t4);
    t4 = ((char*)((ng9)));
    t9 = (t0 + 1368U);
    t10 = *((char **)t9);
    t9 = (t0 + 2168U);
    t11 = *((char **)t9);
    t9 = ((char*)((ng9)));
    t12 = ((char*)((ng9)));
    xsi_vlogtype_concat(t7, 32, 32, 7U, t12, 2, t9, 2, t11, 1, t10, 1, t4, 4, t8, 6, t3, 16);
    t13 = (t0 + 5448);
    xsi_vlogvar_wait_assign_value(t13, t7, 0, 0, 32, 0LL);
    goto LAB49;

LAB17:    xsi_set_current_line(4200, ng0);
    t3 = ((char*)((ng9)));
    t4 = (t0 + 1048U);
    t8 = *((char **)t4);
    t4 = ((char*)((ng9)));
    t9 = (t0 + 1368U);
    t10 = *((char **)t9);
    t9 = (t0 + 2168U);
    t11 = *((char **)t9);
    t9 = ((char*)((ng9)));
    t12 = ((char*)((ng9)));
    xsi_vlogtype_concat(t7, 32, 32, 7U, t12, 2, t9, 2, t11, 1, t10, 1, t4, 4, t8, 6, t3, 16);
    t13 = (t0 + 5448);
    xsi_vlogvar_wait_assign_value(t13, t7, 0, 0, 32, 0LL);
    goto LAB49;

LAB19:    xsi_set_current_line(4201, ng0);
    t3 = ((char*)((ng9)));
    t4 = (t0 + 1048U);
    t8 = *((char **)t4);
    t4 = ((char*)((ng9)));
    t9 = (t0 + 1368U);
    t10 = *((char **)t9);
    t9 = (t0 + 2168U);
    t11 = *((char **)t9);
    t9 = ((char*)((ng9)));
    t12 = ((char*)((ng9)));
    xsi_vlogtype_concat(t7, 32, 32, 7U, t12, 2, t9, 2, t11, 1, t10, 1, t4, 4, t8, 6, t3, 16);
    t13 = (t0 + 5448);
    xsi_vlogvar_wait_assign_value(t13, t7, 0, 0, 32, 0LL);
    goto LAB49;

LAB21:    xsi_set_current_line(4202, ng0);
    t3 = ((char*)((ng9)));
    t4 = (t0 + 1048U);
    t8 = *((char **)t4);
    t4 = ((char*)((ng9)));
    t9 = (t0 + 1368U);
    t10 = *((char **)t9);
    t9 = (t0 + 2168U);
    t11 = *((char **)t9);
    t9 = ((char*)((ng9)));
    t12 = ((char*)((ng9)));
    xsi_vlogtype_concat(t7, 32, 32, 7U, t12, 2, t9, 2, t11, 1, t10, 1, t4, 4, t8, 6, t3, 16);
    t13 = (t0 + 5448);
    xsi_vlogvar_wait_assign_value(t13, t7, 0, 0, 32, 0LL);
    goto LAB49;

LAB23:    xsi_set_current_line(4203, ng0);
    t3 = ((char*)((ng9)));
    t4 = (t0 + 1048U);
    t8 = *((char **)t4);
    t4 = ((char*)((ng9)));
    t9 = (t0 + 1368U);
    t10 = *((char **)t9);
    t9 = (t0 + 2168U);
    t11 = *((char **)t9);
    t9 = ((char*)((ng9)));
    t12 = ((char*)((ng9)));
    xsi_vlogtype_concat(t7, 32, 32, 7U, t12, 2, t9, 2, t11, 1, t10, 1, t4, 4, t8, 6, t3, 16);
    t13 = (t0 + 5448);
    xsi_vlogvar_wait_assign_value(t13, t7, 0, 0, 32, 0LL);
    goto LAB49;

LAB25:    xsi_set_current_line(4204, ng0);
    t3 = ((char*)((ng9)));
    t4 = (t0 + 1048U);
    t8 = *((char **)t4);
    t4 = ((char*)((ng9)));
    t9 = (t0 + 1368U);
    t10 = *((char **)t9);
    t9 = (t0 + 2168U);
    t11 = *((char **)t9);
    t9 = ((char*)((ng9)));
    t12 = ((char*)((ng9)));
    xsi_vlogtype_concat(t7, 32, 32, 7U, t12, 2, t9, 2, t11, 1, t10, 1, t4, 4, t8, 6, t3, 16);
    t13 = (t0 + 5448);
    xsi_vlogvar_wait_assign_value(t13, t7, 0, 0, 32, 0LL);
    goto LAB49;

LAB27:    xsi_set_current_line(4205, ng0);
    t3 = ((char*)((ng9)));
    t4 = (t0 + 1048U);
    t8 = *((char **)t4);
    t4 = ((char*)((ng9)));
    t9 = (t0 + 1368U);
    t10 = *((char **)t9);
    t9 = (t0 + 2168U);
    t11 = *((char **)t9);
    t9 = ((char*)((ng9)));
    t12 = ((char*)((ng9)));
    xsi_vlogtype_concat(t7, 32, 32, 7U, t12, 2, t9, 2, t11, 1, t10, 1, t4, 4, t8, 6, t3, 16);
    t13 = (t0 + 5448);
    xsi_vlogvar_wait_assign_value(t13, t7, 0, 0, 32, 0LL);
    goto LAB49;

LAB29:    xsi_set_current_line(4206, ng0);
    t3 = ((char*)((ng9)));
    t4 = (t0 + 1048U);
    t8 = *((char **)t4);
    t4 = ((char*)((ng9)));
    t9 = (t0 + 1368U);
    t10 = *((char **)t9);
    t9 = (t0 + 2168U);
    t11 = *((char **)t9);
    t9 = ((char*)((ng9)));
    t12 = ((char*)((ng9)));
    xsi_vlogtype_concat(t7, 32, 32, 7U, t12, 2, t9, 2, t11, 1, t10, 1, t4, 4, t8, 6, t3, 16);
    t13 = (t0 + 5448);
    xsi_vlogvar_wait_assign_value(t13, t7, 0, 0, 32, 0LL);
    goto LAB49;

LAB31:    xsi_set_current_line(4207, ng0);
    t3 = ((char*)((ng9)));
    t4 = (t0 + 1048U);
    t8 = *((char **)t4);
    t4 = ((char*)((ng9)));
    t9 = (t0 + 1368U);
    t10 = *((char **)t9);
    t9 = (t0 + 2168U);
    t11 = *((char **)t9);
    t9 = ((char*)((ng9)));
    t12 = ((char*)((ng9)));
    xsi_vlogtype_concat(t7, 32, 32, 7U, t12, 2, t9, 2, t11, 1, t10, 1, t4, 4, t8, 6, t3, 16);
    t13 = (t0 + 5448);
    xsi_vlogvar_wait_assign_value(t13, t7, 0, 0, 32, 0LL);
    goto LAB49;

LAB33:    xsi_set_current_line(4208, ng0);
    t3 = ((char*)((ng9)));
    t4 = (t0 + 1048U);
    t8 = *((char **)t4);
    t4 = ((char*)((ng9)));
    t9 = (t0 + 1368U);
    t10 = *((char **)t9);
    t9 = (t0 + 2168U);
    t11 = *((char **)t9);
    t9 = ((char*)((ng9)));
    t12 = ((char*)((ng9)));
    xsi_vlogtype_concat(t7, 32, 32, 7U, t12, 2, t9, 2, t11, 1, t10, 1, t4, 4, t8, 6, t3, 16);
    t13 = (t0 + 5448);
    xsi_vlogvar_wait_assign_value(t13, t7, 0, 0, 32, 0LL);
    goto LAB49;

LAB35:    xsi_set_current_line(4209, ng0);
    t3 = ((char*)((ng9)));
    t4 = (t0 + 1048U);
    t8 = *((char **)t4);
    t4 = ((char*)((ng9)));
    t9 = (t0 + 1368U);
    t10 = *((char **)t9);
    t9 = (t0 + 2168U);
    t11 = *((char **)t9);
    t9 = ((char*)((ng9)));
    t12 = ((char*)((ng9)));
    xsi_vlogtype_concat(t7, 32, 32, 7U, t12, 2, t9, 2, t11, 1, t10, 1, t4, 4, t8, 6, t3, 16);
    t13 = (t0 + 5448);
    xsi_vlogvar_wait_assign_value(t13, t7, 0, 0, 32, 0LL);
    goto LAB49;

LAB37:    xsi_set_current_line(4210, ng0);
    t3 = ((char*)((ng9)));
    t4 = (t0 + 1048U);
    t8 = *((char **)t4);
    t4 = ((char*)((ng9)));
    t9 = (t0 + 1368U);
    t10 = *((char **)t9);
    t9 = (t0 + 2168U);
    t11 = *((char **)t9);
    t9 = ((char*)((ng9)));
    t12 = ((char*)((ng9)));
    xsi_vlogtype_concat(t7, 32, 32, 7U, t12, 2, t9, 2, t11, 1, t10, 1, t4, 4, t8, 6, t3, 16);
    t13 = (t0 + 5448);
    xsi_vlogvar_wait_assign_value(t13, t7, 0, 0, 32, 0LL);
    goto LAB49;

LAB39:    xsi_set_current_line(4211, ng0);
    t3 = (t0 + 2328U);
    t4 = *((char **)t3);
    memset(t16, 0, 8);
    t3 = (t4 + 4);
    t17 = *((unsigned int *)t3);
    t18 = (~(t17));
    t19 = *((unsigned int *)t4);
    t20 = (t19 & t18);
    t21 = (t20 & 1U);
    if (t21 != 0)
        goto LAB50;

LAB51:    if (*((unsigned int *)t3) != 0)
        goto LAB52;

LAB53:    t9 = (t16 + 4);
    t22 = *((unsigned int *)t16);
    t23 = *((unsigned int *)t9);
    t24 = (t22 || t23);
    if (t24 > 0)
        goto LAB54;

LAB55:    t26 = *((unsigned int *)t16);
    t27 = (~(t26));
    t28 = *((unsigned int *)t9);
    t29 = (t27 || t28);
    if (t29 > 0)
        goto LAB56;

LAB57:    if (*((unsigned int *)t9) > 0)
        goto LAB58;

LAB59:    if (*((unsigned int *)t16) > 0)
        goto LAB60;

LAB61:    memcpy(t7, t30, 8);

LAB62:    t15 = (t0 + 5448);
    xsi_vlogvar_wait_assign_value(t15, t7, 0, 0, 32, 0LL);
    goto LAB49;

LAB41:    xsi_set_current_line(4212, ng0);
    t3 = (t0 + 2328U);
    t4 = *((char **)t3);
    memset(t16, 0, 8);
    t3 = (t4 + 4);
    t17 = *((unsigned int *)t3);
    t18 = (~(t17));
    t19 = *((unsigned int *)t4);
    t20 = (t19 & t18);
    t21 = (t20 & 1U);
    if (t21 != 0)
        goto LAB63;

LAB64:    if (*((unsigned int *)t3) != 0)
        goto LAB65;

LAB66:    t9 = (t16 + 4);
    t22 = *((unsigned int *)t16);
    t23 = *((unsigned int *)t9);
    t24 = (t22 || t23);
    if (t24 > 0)
        goto LAB67;

LAB68:    t26 = *((unsigned int *)t16);
    t27 = (~(t26));
    t28 = *((unsigned int *)t9);
    t29 = (t27 || t28);
    if (t29 > 0)
        goto LAB69;

LAB70:    if (*((unsigned int *)t9) > 0)
        goto LAB71;

LAB72:    if (*((unsigned int *)t16) > 0)
        goto LAB73;

LAB74:    memcpy(t7, t30, 8);

LAB75:    t15 = (t0 + 5448);
    xsi_vlogvar_wait_assign_value(t15, t7, 0, 0, 32, 0LL);
    goto LAB49;

LAB43:    xsi_set_current_line(4213, ng0);
    t3 = (t0 + 2328U);
    t4 = *((char **)t3);
    memset(t16, 0, 8);
    t3 = (t4 + 4);
    t17 = *((unsigned int *)t3);
    t18 = (~(t17));
    t19 = *((unsigned int *)t4);
    t20 = (t19 & t18);
    t21 = (t20 & 1U);
    if (t21 != 0)
        goto LAB76;

LAB77:    if (*((unsigned int *)t3) != 0)
        goto LAB78;

LAB79:    t9 = (t16 + 4);
    t22 = *((unsigned int *)t16);
    t23 = *((unsigned int *)t9);
    t24 = (t22 || t23);
    if (t24 > 0)
        goto LAB80;

LAB81:    t26 = *((unsigned int *)t16);
    t27 = (~(t26));
    t28 = *((unsigned int *)t9);
    t29 = (t27 || t28);
    if (t29 > 0)
        goto LAB82;

LAB83:    if (*((unsigned int *)t9) > 0)
        goto LAB84;

LAB85:    if (*((unsigned int *)t16) > 0)
        goto LAB86;

LAB87:    memcpy(t7, t12, 8);

LAB88:    t10 = (t0 + 5448);
    xsi_vlogvar_wait_assign_value(t10, t7, 0, 0, 32, 0LL);
    goto LAB49;

LAB45:    xsi_set_current_line(4214, ng0);
    t3 = (t0 + 2328U);
    t4 = *((char **)t3);
    memset(t16, 0, 8);
    t3 = (t4 + 4);
    t17 = *((unsigned int *)t3);
    t18 = (~(t17));
    t19 = *((unsigned int *)t4);
    t20 = (t19 & t18);
    t21 = (t20 & 1U);
    if (t21 != 0)
        goto LAB89;

LAB90:    if (*((unsigned int *)t3) != 0)
        goto LAB91;

LAB92:    t9 = (t16 + 4);
    t22 = *((unsigned int *)t16);
    t23 = *((unsigned int *)t9);
    t24 = (t22 || t23);
    if (t24 > 0)
        goto LAB93;

LAB94:    t26 = *((unsigned int *)t16);
    t27 = (~(t26));
    t28 = *((unsigned int *)t9);
    t29 = (t27 || t28);
    if (t29 > 0)
        goto LAB95;

LAB96:    if (*((unsigned int *)t9) > 0)
        goto LAB97;

LAB98:    if (*((unsigned int *)t16) > 0)
        goto LAB99;

LAB100:    memcpy(t7, t12, 8);

LAB101:    t10 = (t0 + 5448);
    xsi_vlogvar_wait_assign_value(t10, t7, 0, 0, 32, 0LL);
    goto LAB49;

LAB50:    *((unsigned int *)t16) = 1;
    goto LAB53;

LAB52:    t8 = (t16 + 4);
    *((unsigned int *)t16) = 1;
    *((unsigned int *)t8) = 1;
    goto LAB53;

LAB54:    t10 = (t0 + 4248U);
    t11 = *((char **)t10);
    t10 = (t0 + 3768U);
    t12 = *((char **)t10);
    t10 = ((char*)((ng9)));
    t13 = ((char*)((ng9)));
    t14 = ((char*)((ng10)));
    xsi_vlogtype_concat(t25, 32, 32, 5U, t14, 2, t13, 2, t10, 4, t12, 8, t11, 16);
    goto LAB55;

LAB56:    t15 = (t0 + 4088U);
    t30 = *((char **)t15);
    goto LAB57;

LAB58:    xsi_vlog_unsigned_bit_combine(t7, 32, t25, 32, t30, 32);
    goto LAB62;

LAB60:    memcpy(t7, t25, 8);
    goto LAB62;

LAB63:    *((unsigned int *)t16) = 1;
    goto LAB66;

LAB65:    t8 = (t16 + 4);
    *((unsigned int *)t16) = 1;
    *((unsigned int *)t8) = 1;
    goto LAB66;

LAB67:    t10 = (t0 + 4408U);
    t11 = *((char **)t10);
    t10 = (t0 + 3928U);
    t12 = *((char **)t10);
    t10 = ((char*)((ng9)));
    t13 = ((char*)((ng9)));
    t14 = ((char*)((ng10)));
    xsi_vlogtype_concat(t25, 32, 32, 5U, t14, 2, t13, 2, t10, 4, t12, 8, t11, 16);
    goto LAB68;

LAB69:    t15 = (t0 + 4088U);
    t30 = *((char **)t15);
    goto LAB70;

LAB71:    xsi_vlog_unsigned_bit_combine(t7, 32, t25, 32, t30, 32);
    goto LAB75;

LAB73:    memcpy(t7, t25, 8);
    goto LAB75;

LAB76:    *((unsigned int *)t16) = 1;
    goto LAB79;

LAB78:    t8 = (t16 + 4);
    *((unsigned int *)t16) = 1;
    *((unsigned int *)t8) = 1;
    goto LAB79;

LAB80:    t10 = (t0 + 4888U);
    t11 = *((char **)t10);
    goto LAB81;

LAB82:    t10 = (t0 + 4088U);
    t12 = *((char **)t10);
    goto LAB83;

LAB84:    xsi_vlog_unsigned_bit_combine(t7, 32, t11, 32, t12, 32);
    goto LAB88;

LAB86:    memcpy(t7, t11, 8);
    goto LAB88;

LAB89:    *((unsigned int *)t16) = 1;
    goto LAB92;

LAB91:    t8 = (t16 + 4);
    *((unsigned int *)t16) = 1;
    *((unsigned int *)t8) = 1;
    goto LAB92;

LAB93:    t10 = (t0 + 5048U);
    t11 = *((char **)t10);
    goto LAB94;

LAB95:    t10 = (t0 + 4088U);
    t12 = *((char **)t10);
    goto LAB96;

LAB97:    xsi_vlog_unsigned_bit_combine(t7, 32, t11, 32, t12, 32);
    goto LAB101;

LAB99:    memcpy(t7, t11, 8);
    goto LAB101;

}


extern void work_m_00000000001659419000_0718496438_init()
{
	static char *pe[] = {(void *)Cont_4160_0,(void *)Cont_4161_1,(void *)Cont_4162_2,(void *)Cont_4166_3,(void *)Cont_4170_4,(void *)Cont_4173_5,(void *)Cont_4177_6,(void *)Cont_4178_7,(void *)Cont_4179_8,(void *)Cont_4180_9,(void *)Cont_4185_10,(void *)Cont_4190_11,(void *)Always_4193_12};
	xsi_register_didat("work_m_00000000001659419000_0718496438", "isim/main_isim_beh.exe.sim/work/m_00000000001659419000_0718496438.didat");
	xsi_register_executes(pe);
}
