# GUI

** C++/Qt source code for different interfaces to the FPGA used for the RHS or RHD evaluation systems. **

## Description
Each re-configuration of the main.bit binary file that is made when reconfiguring the Verilog hardware code has accompanying interface changes. This sub-directory is where those corresponding software changes are kept (for now).