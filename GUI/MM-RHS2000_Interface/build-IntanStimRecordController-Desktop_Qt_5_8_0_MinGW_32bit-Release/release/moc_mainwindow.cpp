/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../source/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[133];
    char stringdata0[2264];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 12), // "focusChanged"
QT_MOC_LITERAL(2, 24, 0), // ""
QT_MOC_LITERAL(3, 25, 8), // "QWidget*"
QT_MOC_LITERAL(4, 34, 9), // "lostFocus"
QT_MOC_LITERAL(5, 44, 11), // "gainedFocus"
QT_MOC_LITERAL(6, 56, 18), // "notifyFocusChanged"
QT_MOC_LITERAL(7, 75, 18), // "copyStimParameters"
QT_MOC_LITERAL(8, 94, 19), // "pasteStimParameters"
QT_MOC_LITERAL(9, 114, 5), // "about"
QT_MOC_LITERAL(10, 120, 21), // "keyboardShortcutsHelp"
QT_MOC_LITERAL(11, 142, 15), // "chipFiltersHelp"
QT_MOC_LITERAL(12, 158, 15), // "comparatorsHelp"
QT_MOC_LITERAL(13, 174, 8), // "dacsHelp"
QT_MOC_LITERAL(14, 183, 18), // "highpassFilterHelp"
QT_MOC_LITERAL(15, 202, 14), // "fastSettleHelp"
QT_MOC_LITERAL(16, 217, 14), // "ioExpanderHelp"
QT_MOC_LITERAL(17, 232, 16), // "openIntanWebsite"
QT_MOC_LITERAL(18, 249, 17), // "runInterfaceBoard"
QT_MOC_LITERAL(19, 267, 20), // "recordInterfaceBoard"
QT_MOC_LITERAL(20, 288, 27), // "triggerRecordInterfaceBoard"
QT_MOC_LITERAL(21, 316, 18), // "stopInterfaceBoard"
QT_MOC_LITERAL(22, 335, 22), // "selectBaseFilenameSlot"
QT_MOC_LITERAL(23, 358, 15), // "changeNumFrames"
QT_MOC_LITERAL(24, 374, 5), // "index"
QT_MOC_LITERAL(25, 380, 12), // "changeYScale"
QT_MOC_LITERAL(26, 393, 17), // "changeYScaleDcAmp"
QT_MOC_LITERAL(27, 411, 15), // "changeYScaleAdc"
QT_MOC_LITERAL(28, 427, 13), // "changeAmpType"
QT_MOC_LITERAL(29, 441, 12), // "changeTScale"
QT_MOC_LITERAL(30, 454, 16), // "changeSampleRate"
QT_MOC_LITERAL(31, 471, 15), // "sampleRateIndex"
QT_MOC_LITERAL(32, 487, 16), // "updateStimParams"
QT_MOC_LITERAL(33, 504, 20), // "enableHighpassFilter"
QT_MOC_LITERAL(34, 525, 6), // "enable"
QT_MOC_LITERAL(35, 532, 19), // "enableLowpassFilter"
QT_MOC_LITERAL(36, 552, 24), // "dacFilterLineEditChanged"
QT_MOC_LITERAL(37, 577, 15), // "changeBandwidth"
QT_MOC_LITERAL(38, 593, 24), // "changeImpedanceFrequency"
QT_MOC_LITERAL(39, 618, 10), // "changePort"
QT_MOC_LITERAL(40, 629, 4), // "port"
QT_MOC_LITERAL(41, 634, 13), // "changeDacGain"
QT_MOC_LITERAL(42, 648, 22), // "changeDacNoiseSuppress"
QT_MOC_LITERAL(43, 671, 9), // "dacEnable"
QT_MOC_LITERAL(44, 681, 13), // "dacSetChannel"
QT_MOC_LITERAL(45, 695, 11), // "dacSelected"
QT_MOC_LITERAL(46, 707, 10), // "dacChannel"
QT_MOC_LITERAL(47, 718, 13), // "renameChannel"
QT_MOC_LITERAL(48, 732, 20), // "sortChannelsByNumber"
QT_MOC_LITERAL(49, 753, 18), // "sortChannelsByName"
QT_MOC_LITERAL(50, 772, 27), // "restoreOriginalChannelOrder"
QT_MOC_LITERAL(51, 800, 19), // "alphabetizeChannels"
QT_MOC_LITERAL(52, 820, 19), // "toggleChannelEnable"
QT_MOC_LITERAL(53, 840, 17), // "enableAllChannels"
QT_MOC_LITERAL(54, 858, 18), // "disableAllChannels"
QT_MOC_LITERAL(55, 877, 10), // "spikeScope"
QT_MOC_LITERAL(56, 888, 18), // "newSelectedChannel"
QT_MOC_LITERAL(57, 907, 14), // "SignalChannel*"
QT_MOC_LITERAL(58, 922, 10), // "newChannel"
QT_MOC_LITERAL(59, 933, 9), // "scanPorts"
QT_MOC_LITERAL(60, 943, 12), // "loadSettings"
QT_MOC_LITERAL(61, 956, 12), // "saveSettings"
QT_MOC_LITERAL(62, 969, 16), // "loadStimSettings"
QT_MOC_LITERAL(63, 986, 16), // "saveStimSettings"
QT_MOC_LITERAL(64, 1003, 14), // "showImpedances"
QT_MOC_LITERAL(65, 1018, 7), // "enabled"
QT_MOC_LITERAL(66, 1026, 14), // "saveImpedances"
QT_MOC_LITERAL(67, 1041, 23), // "runImpedanceMeasurement"
QT_MOC_LITERAL(68, 1065, 23), // "manualCableDelayControl"
QT_MOC_LITERAL(69, 1089, 14), // "plotPointsMode"
QT_MOC_LITERAL(70, 1104, 19), // "setSaveFormatDialog"
QT_MOC_LITERAL(71, 1124, 16), // "setDacThreshold1"
QT_MOC_LITERAL(72, 1141, 9), // "threshold"
QT_MOC_LITERAL(73, 1151, 16), // "setDacThreshold2"
QT_MOC_LITERAL(74, 1168, 16), // "setDacThreshold3"
QT_MOC_LITERAL(75, 1185, 16), // "setDacThreshold4"
QT_MOC_LITERAL(76, 1202, 16), // "setDacThreshold5"
QT_MOC_LITERAL(77, 1219, 16), // "setDacThreshold6"
QT_MOC_LITERAL(78, 1236, 16), // "setDacThreshold7"
QT_MOC_LITERAL(79, 1253, 16), // "setDacThreshold8"
QT_MOC_LITERAL(80, 1270, 17), // "setDac1WindowStop"
QT_MOC_LITERAL(81, 1288, 6), // "sample"
QT_MOC_LITERAL(82, 1295, 18), // "setDac2WindowStart"
QT_MOC_LITERAL(83, 1314, 17), // "setDac2WindowStop"
QT_MOC_LITERAL(84, 1332, 18), // "setDac3WindowStart"
QT_MOC_LITERAL(85, 1351, 17), // "setDac3WindowStop"
QT_MOC_LITERAL(86, 1369, 18), // "setDac4WindowStart"
QT_MOC_LITERAL(87, 1388, 17), // "setDac4WindowStop"
QT_MOC_LITERAL(88, 1406, 18), // "setDac5WindowStart"
QT_MOC_LITERAL(89, 1425, 17), // "setDac5WindowStop"
QT_MOC_LITERAL(90, 1443, 18), // "setDac6WindowStart"
QT_MOC_LITERAL(91, 1462, 17), // "setDac6WindowStop"
QT_MOC_LITERAL(92, 1480, 18), // "setDac7WindowStart"
QT_MOC_LITERAL(93, 1499, 17), // "setDac7WindowStop"
QT_MOC_LITERAL(94, 1517, 18), // "setDac8WindowStart"
QT_MOC_LITERAL(95, 1536, 17), // "setDac8WindowStop"
QT_MOC_LITERAL(96, 1554, 20), // "setDac1ThresholdType"
QT_MOC_LITERAL(97, 1575, 13), // "thresholdType"
QT_MOC_LITERAL(98, 1589, 20), // "setDac2ThresholdType"
QT_MOC_LITERAL(99, 1610, 20), // "setDac3ThresholdType"
QT_MOC_LITERAL(100, 1631, 20), // "setDac4ThresholdType"
QT_MOC_LITERAL(101, 1652, 20), // "setDac5ThresholdType"
QT_MOC_LITERAL(102, 1673, 20), // "setDac6ThresholdType"
QT_MOC_LITERAL(103, 1694, 20), // "setDac7ThresholdType"
QT_MOC_LITERAL(104, 1715, 20), // "setDac8ThresholdType"
QT_MOC_LITERAL(105, 1736, 21), // "changeDetectionMethod"
QT_MOC_LITERAL(106, 1758, 19), // "dacUpdateParameters"
QT_MOC_LITERAL(107, 1778, 25), // "setDiscreteEventPlotState"
QT_MOC_LITERAL(108, 1804, 21), // "setWaveEventPlotState"
QT_MOC_LITERAL(109, 1826, 26), // "hpEvtFilterLineEditChanged"
QT_MOC_LITERAL(110, 1853, 26), // "lpEvtFilterLineEditChanged"
QT_MOC_LITERAL(111, 1880, 17), // "enablehpEvtFilter"
QT_MOC_LITERAL(112, 1898, 17), // "enablelpEvtFilter"
QT_MOC_LITERAL(113, 1916, 17), // "evtFilterSelected"
QT_MOC_LITERAL(114, 1934, 11), // "buttonIndex"
QT_MOC_LITERAL(115, 1946, 17), // "dacFilterSelected"
QT_MOC_LITERAL(116, 1964, 27), // "referenceSetSelectedChannel"
QT_MOC_LITERAL(117, 1992, 20), // "referenceSetHardware"
QT_MOC_LITERAL(118, 2013, 13), // "referenceHelp"
QT_MOC_LITERAL(119, 2027, 9), // "stimParam"
QT_MOC_LITERAL(120, 2037, 31), // "setDigitalOutSequenceParameters"
QT_MOC_LITERAL(121, 2069, 17), // "Rhs2000EvalBoard*"
QT_MOC_LITERAL(122, 2087, 9), // "evalBoard"
QT_MOC_LITERAL(123, 2097, 11), // "timestep_us"
QT_MOC_LITERAL(124, 2109, 7), // "channel"
QT_MOC_LITERAL(125, 2117, 15), // "StimParameters*"
QT_MOC_LITERAL(126, 2133, 10), // "parameters"
QT_MOC_LITERAL(127, 2144, 30), // "setAnalogOutSequenceParameters"
QT_MOC_LITERAL(128, 2175, 25), // "setStimSequenceParameters"
QT_MOC_LITERAL(129, 2201, 14), // "currentstep_uA"
QT_MOC_LITERAL(130, 2216, 6), // "stream"
QT_MOC_LITERAL(131, 2223, 17), // "ampSettleSettings"
QT_MOC_LITERAL(132, 2241, 22) // "chargeRecoverySettings"

    },
    "MainWindow\0focusChanged\0\0QWidget*\0"
    "lostFocus\0gainedFocus\0notifyFocusChanged\0"
    "copyStimParameters\0pasteStimParameters\0"
    "about\0keyboardShortcutsHelp\0chipFiltersHelp\0"
    "comparatorsHelp\0dacsHelp\0highpassFilterHelp\0"
    "fastSettleHelp\0ioExpanderHelp\0"
    "openIntanWebsite\0runInterfaceBoard\0"
    "recordInterfaceBoard\0triggerRecordInterfaceBoard\0"
    "stopInterfaceBoard\0selectBaseFilenameSlot\0"
    "changeNumFrames\0index\0changeYScale\0"
    "changeYScaleDcAmp\0changeYScaleAdc\0"
    "changeAmpType\0changeTScale\0changeSampleRate\0"
    "sampleRateIndex\0updateStimParams\0"
    "enableHighpassFilter\0enable\0"
    "enableLowpassFilter\0dacFilterLineEditChanged\0"
    "changeBandwidth\0changeImpedanceFrequency\0"
    "changePort\0port\0changeDacGain\0"
    "changeDacNoiseSuppress\0dacEnable\0"
    "dacSetChannel\0dacSelected\0dacChannel\0"
    "renameChannel\0sortChannelsByNumber\0"
    "sortChannelsByName\0restoreOriginalChannelOrder\0"
    "alphabetizeChannels\0toggleChannelEnable\0"
    "enableAllChannels\0disableAllChannels\0"
    "spikeScope\0newSelectedChannel\0"
    "SignalChannel*\0newChannel\0scanPorts\0"
    "loadSettings\0saveSettings\0loadStimSettings\0"
    "saveStimSettings\0showImpedances\0enabled\0"
    "saveImpedances\0runImpedanceMeasurement\0"
    "manualCableDelayControl\0plotPointsMode\0"
    "setSaveFormatDialog\0setDacThreshold1\0"
    "threshold\0setDacThreshold2\0setDacThreshold3\0"
    "setDacThreshold4\0setDacThreshold5\0"
    "setDacThreshold6\0setDacThreshold7\0"
    "setDacThreshold8\0setDac1WindowStop\0"
    "sample\0setDac2WindowStart\0setDac2WindowStop\0"
    "setDac3WindowStart\0setDac3WindowStop\0"
    "setDac4WindowStart\0setDac4WindowStop\0"
    "setDac5WindowStart\0setDac5WindowStop\0"
    "setDac6WindowStart\0setDac6WindowStop\0"
    "setDac7WindowStart\0setDac7WindowStop\0"
    "setDac8WindowStart\0setDac8WindowStop\0"
    "setDac1ThresholdType\0thresholdType\0"
    "setDac2ThresholdType\0setDac3ThresholdType\0"
    "setDac4ThresholdType\0setDac5ThresholdType\0"
    "setDac6ThresholdType\0setDac7ThresholdType\0"
    "setDac8ThresholdType\0changeDetectionMethod\0"
    "dacUpdateParameters\0setDiscreteEventPlotState\0"
    "setWaveEventPlotState\0hpEvtFilterLineEditChanged\0"
    "lpEvtFilterLineEditChanged\0enablehpEvtFilter\0"
    "enablelpEvtFilter\0evtFilterSelected\0"
    "buttonIndex\0dacFilterSelected\0"
    "referenceSetSelectedChannel\0"
    "referenceSetHardware\0referenceHelp\0"
    "stimParam\0setDigitalOutSequenceParameters\0"
    "Rhs2000EvalBoard*\0evalBoard\0timestep_us\0"
    "channel\0StimParameters*\0parameters\0"
    "setAnalogOutSequenceParameters\0"
    "setStimSequenceParameters\0currentstep_uA\0"
    "stream\0ampSettleSettings\0"
    "chargeRecoverySettings"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
     107,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,  549,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    2,  554,    2, 0x0a /* Public */,
       7,    0,  559,    2, 0x0a /* Public */,
       8,    0,  560,    2, 0x0a /* Public */,
       9,    0,  561,    2, 0x08 /* Private */,
      10,    0,  562,    2, 0x08 /* Private */,
      11,    0,  563,    2, 0x08 /* Private */,
      12,    0,  564,    2, 0x08 /* Private */,
      13,    0,  565,    2, 0x08 /* Private */,
      14,    0,  566,    2, 0x08 /* Private */,
      15,    0,  567,    2, 0x08 /* Private */,
      16,    0,  568,    2, 0x08 /* Private */,
      17,    0,  569,    2, 0x08 /* Private */,
      18,    0,  570,    2, 0x08 /* Private */,
      19,    0,  571,    2, 0x08 /* Private */,
      20,    0,  572,    2, 0x08 /* Private */,
      21,    0,  573,    2, 0x08 /* Private */,
      22,    0,  574,    2, 0x08 /* Private */,
      23,    1,  575,    2, 0x08 /* Private */,
      25,    1,  578,    2, 0x08 /* Private */,
      26,    1,  581,    2, 0x08 /* Private */,
      27,    1,  584,    2, 0x08 /* Private */,
      28,    1,  587,    2, 0x08 /* Private */,
      29,    1,  590,    2, 0x08 /* Private */,
      30,    2,  593,    2, 0x08 /* Private */,
      33,    1,  598,    2, 0x08 /* Private */,
      35,    1,  601,    2, 0x08 /* Private */,
      36,    0,  604,    2, 0x08 /* Private */,
      37,    0,  605,    2, 0x08 /* Private */,
      38,    0,  606,    2, 0x08 /* Private */,
      39,    1,  607,    2, 0x08 /* Private */,
      41,    1,  610,    2, 0x08 /* Private */,
      42,    1,  613,    2, 0x08 /* Private */,
      43,    1,  616,    2, 0x08 /* Private */,
      44,    0,  619,    2, 0x08 /* Private */,
      45,    1,  620,    2, 0x08 /* Private */,
      47,    0,  623,    2, 0x08 /* Private */,
      48,    0,  624,    2, 0x08 /* Private */,
      49,    0,  625,    2, 0x08 /* Private */,
      50,    0,  626,    2, 0x08 /* Private */,
      51,    0,  627,    2, 0x08 /* Private */,
      52,    0,  628,    2, 0x08 /* Private */,
      53,    0,  629,    2, 0x08 /* Private */,
      54,    0,  630,    2, 0x08 /* Private */,
      55,    0,  631,    2, 0x08 /* Private */,
      56,    1,  632,    2, 0x08 /* Private */,
      59,    0,  635,    2, 0x08 /* Private */,
      60,    0,  636,    2, 0x08 /* Private */,
      61,    0,  637,    2, 0x08 /* Private */,
      62,    0,  638,    2, 0x08 /* Private */,
      63,    0,  639,    2, 0x08 /* Private */,
      64,    1,  640,    2, 0x08 /* Private */,
      66,    0,  643,    2, 0x08 /* Private */,
      67,    0,  644,    2, 0x08 /* Private */,
      68,    0,  645,    2, 0x08 /* Private */,
      69,    1,  646,    2, 0x08 /* Private */,
      70,    0,  649,    2, 0x08 /* Private */,
      71,    1,  650,    2, 0x08 /* Private */,
      73,    1,  653,    2, 0x08 /* Private */,
      74,    1,  656,    2, 0x08 /* Private */,
      75,    1,  659,    2, 0x08 /* Private */,
      76,    1,  662,    2, 0x08 /* Private */,
      77,    1,  665,    2, 0x08 /* Private */,
      78,    1,  668,    2, 0x08 /* Private */,
      79,    1,  671,    2, 0x08 /* Private */,
      80,    1,  674,    2, 0x08 /* Private */,
      82,    1,  677,    2, 0x08 /* Private */,
      83,    1,  680,    2, 0x08 /* Private */,
      84,    1,  683,    2, 0x08 /* Private */,
      85,    1,  686,    2, 0x08 /* Private */,
      86,    1,  689,    2, 0x08 /* Private */,
      87,    1,  692,    2, 0x08 /* Private */,
      88,    1,  695,    2, 0x08 /* Private */,
      89,    1,  698,    2, 0x08 /* Private */,
      90,    1,  701,    2, 0x08 /* Private */,
      91,    1,  704,    2, 0x08 /* Private */,
      92,    1,  707,    2, 0x08 /* Private */,
      93,    1,  710,    2, 0x08 /* Private */,
      94,    1,  713,    2, 0x08 /* Private */,
      95,    1,  716,    2, 0x08 /* Private */,
      96,    1,  719,    2, 0x08 /* Private */,
      98,    1,  722,    2, 0x08 /* Private */,
      99,    1,  725,    2, 0x08 /* Private */,
     100,    1,  728,    2, 0x08 /* Private */,
     101,    1,  731,    2, 0x08 /* Private */,
     102,    1,  734,    2, 0x08 /* Private */,
     103,    1,  737,    2, 0x08 /* Private */,
     104,    1,  740,    2, 0x08 /* Private */,
     105,    1,  743,    2, 0x08 /* Private */,
     106,    0,  746,    2, 0x08 /* Private */,
     107,    1,  747,    2, 0x08 /* Private */,
     108,    1,  750,    2, 0x08 /* Private */,
     109,    0,  753,    2, 0x08 /* Private */,
     110,    0,  754,    2, 0x08 /* Private */,
     111,    1,  755,    2, 0x08 /* Private */,
     112,    1,  758,    2, 0x08 /* Private */,
     113,    1,  761,    2, 0x08 /* Private */,
     115,    1,  764,    2, 0x08 /* Private */,
     116,    0,  767,    2, 0x08 /* Private */,
     117,    0,  768,    2, 0x08 /* Private */,
     118,    0,  769,    2, 0x08 /* Private */,
     119,    0,  770,    2, 0x08 /* Private */,
     120,    4,  771,    2, 0x08 /* Private */,
     127,    4,  780,    2, 0x08 /* Private */,
     128,    6,  789,    2, 0x08 /* Private */,
     131,    0,  802,    2, 0x08 /* Private */,
     132,    0,  803,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3, 0x80000000 | 3,    4,    5,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3, 0x80000000 | 3,    4,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   24,
    QMetaType::Void, QMetaType::Int,   24,
    QMetaType::Void, QMetaType::Int,   24,
    QMetaType::Void, QMetaType::Int,   24,
    QMetaType::Void, QMetaType::Int,   24,
    QMetaType::Void, QMetaType::Int,   24,
    QMetaType::Void, QMetaType::Int, QMetaType::Bool,   31,   32,
    QMetaType::Void, QMetaType::Bool,   34,
    QMetaType::Void, QMetaType::Bool,   34,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   40,
    QMetaType::Void, QMetaType::Int,   24,
    QMetaType::Void, QMetaType::Int,   24,
    QMetaType::Void, QMetaType::Bool,   34,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   46,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 57,   58,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   65,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   65,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   72,
    QMetaType::Void, QMetaType::Int,   72,
    QMetaType::Void, QMetaType::Int,   72,
    QMetaType::Void, QMetaType::Int,   72,
    QMetaType::Void, QMetaType::Int,   72,
    QMetaType::Void, QMetaType::Int,   72,
    QMetaType::Void, QMetaType::Int,   72,
    QMetaType::Void, QMetaType::Int,   72,
    QMetaType::Void, QMetaType::Int,   81,
    QMetaType::Void, QMetaType::Int,   81,
    QMetaType::Void, QMetaType::Int,   81,
    QMetaType::Void, QMetaType::Int,   81,
    QMetaType::Void, QMetaType::Int,   81,
    QMetaType::Void, QMetaType::Int,   81,
    QMetaType::Void, QMetaType::Int,   81,
    QMetaType::Void, QMetaType::Int,   81,
    QMetaType::Void, QMetaType::Int,   81,
    QMetaType::Void, QMetaType::Int,   81,
    QMetaType::Void, QMetaType::Int,   81,
    QMetaType::Void, QMetaType::Int,   81,
    QMetaType::Void, QMetaType::Int,   81,
    QMetaType::Void, QMetaType::Int,   81,
    QMetaType::Void, QMetaType::Int,   81,
    QMetaType::Void, QMetaType::Int,   97,
    QMetaType::Void, QMetaType::Int,   97,
    QMetaType::Void, QMetaType::Int,   97,
    QMetaType::Void, QMetaType::Int,   97,
    QMetaType::Void, QMetaType::Int,   97,
    QMetaType::Void, QMetaType::Int,   97,
    QMetaType::Void, QMetaType::Int,   97,
    QMetaType::Void, QMetaType::Int,   97,
    QMetaType::Void, QMetaType::Bool,   34,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   34,
    QMetaType::Void, QMetaType::Bool,   34,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   34,
    QMetaType::Void, QMetaType::Bool,   34,
    QMetaType::Void, QMetaType::Int,  114,
    QMetaType::Void, QMetaType::Int,   24,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 121, QMetaType::Double, QMetaType::Int, 0x80000000 | 125,  122,  123,  124,  126,
    QMetaType::Void, 0x80000000 | 121, QMetaType::Double, QMetaType::Int, 0x80000000 | 125,  122,  123,  124,  126,
    QMetaType::Void, 0x80000000 | 121, QMetaType::Double, QMetaType::Double, QMetaType::Int, QMetaType::Int, 0x80000000 | 125,  122,  123,  129,  130,  124,  126,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->focusChanged((*reinterpret_cast< QWidget*(*)>(_a[1])),(*reinterpret_cast< QWidget*(*)>(_a[2]))); break;
        case 1: _t->notifyFocusChanged((*reinterpret_cast< QWidget*(*)>(_a[1])),(*reinterpret_cast< QWidget*(*)>(_a[2]))); break;
        case 2: _t->copyStimParameters(); break;
        case 3: _t->pasteStimParameters(); break;
        case 4: _t->about(); break;
        case 5: _t->keyboardShortcutsHelp(); break;
        case 6: _t->chipFiltersHelp(); break;
        case 7: _t->comparatorsHelp(); break;
        case 8: _t->dacsHelp(); break;
        case 9: _t->highpassFilterHelp(); break;
        case 10: _t->fastSettleHelp(); break;
        case 11: _t->ioExpanderHelp(); break;
        case 12: _t->openIntanWebsite(); break;
        case 13: _t->runInterfaceBoard(); break;
        case 14: _t->recordInterfaceBoard(); break;
        case 15: _t->triggerRecordInterfaceBoard(); break;
        case 16: _t->stopInterfaceBoard(); break;
        case 17: _t->selectBaseFilenameSlot(); break;
        case 18: _t->changeNumFrames((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 19: _t->changeYScale((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 20: _t->changeYScaleDcAmp((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 21: _t->changeYScaleAdc((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 22: _t->changeAmpType((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 23: _t->changeTScale((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 24: _t->changeSampleRate((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 25: _t->enableHighpassFilter((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 26: _t->enableLowpassFilter((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 27: _t->dacFilterLineEditChanged(); break;
        case 28: _t->changeBandwidth(); break;
        case 29: _t->changeImpedanceFrequency(); break;
        case 30: _t->changePort((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 31: _t->changeDacGain((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 32: _t->changeDacNoiseSuppress((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 33: _t->dacEnable((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 34: _t->dacSetChannel(); break;
        case 35: _t->dacSelected((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 36: _t->renameChannel(); break;
        case 37: _t->sortChannelsByNumber(); break;
        case 38: _t->sortChannelsByName(); break;
        case 39: _t->restoreOriginalChannelOrder(); break;
        case 40: _t->alphabetizeChannels(); break;
        case 41: _t->toggleChannelEnable(); break;
        case 42: _t->enableAllChannels(); break;
        case 43: _t->disableAllChannels(); break;
        case 44: _t->spikeScope(); break;
        case 45: _t->newSelectedChannel((*reinterpret_cast< SignalChannel*(*)>(_a[1]))); break;
        case 46: _t->scanPorts(); break;
        case 47: _t->loadSettings(); break;
        case 48: _t->saveSettings(); break;
        case 49: _t->loadStimSettings(); break;
        case 50: _t->saveStimSettings(); break;
        case 51: _t->showImpedances((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 52: _t->saveImpedances(); break;
        case 53: _t->runImpedanceMeasurement(); break;
        case 54: _t->manualCableDelayControl(); break;
        case 55: _t->plotPointsMode((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 56: _t->setSaveFormatDialog(); break;
        case 57: _t->setDacThreshold1((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 58: _t->setDacThreshold2((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 59: _t->setDacThreshold3((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 60: _t->setDacThreshold4((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 61: _t->setDacThreshold5((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 62: _t->setDacThreshold6((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 63: _t->setDacThreshold7((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 64: _t->setDacThreshold8((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 65: _t->setDac1WindowStop((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 66: _t->setDac2WindowStart((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 67: _t->setDac2WindowStop((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 68: _t->setDac3WindowStart((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 69: _t->setDac3WindowStop((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 70: _t->setDac4WindowStart((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 71: _t->setDac4WindowStop((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 72: _t->setDac5WindowStart((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 73: _t->setDac5WindowStop((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 74: _t->setDac6WindowStart((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 75: _t->setDac6WindowStop((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 76: _t->setDac7WindowStart((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 77: _t->setDac7WindowStop((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 78: _t->setDac8WindowStart((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 79: _t->setDac8WindowStop((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 80: _t->setDac1ThresholdType((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 81: _t->setDac2ThresholdType((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 82: _t->setDac3ThresholdType((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 83: _t->setDac4ThresholdType((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 84: _t->setDac5ThresholdType((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 85: _t->setDac6ThresholdType((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 86: _t->setDac7ThresholdType((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 87: _t->setDac8ThresholdType((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 88: _t->changeDetectionMethod((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 89: _t->dacUpdateParameters(); break;
        case 90: _t->setDiscreteEventPlotState((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 91: _t->setWaveEventPlotState((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 92: _t->hpEvtFilterLineEditChanged(); break;
        case 93: _t->lpEvtFilterLineEditChanged(); break;
        case 94: _t->enablehpEvtFilter((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 95: _t->enablelpEvtFilter((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 96: _t->evtFilterSelected((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 97: _t->dacFilterSelected((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 98: _t->referenceSetSelectedChannel(); break;
        case 99: _t->referenceSetHardware(); break;
        case 100: _t->referenceHelp(); break;
        case 101: _t->stimParam(); break;
        case 102: _t->setDigitalOutSequenceParameters((*reinterpret_cast< Rhs2000EvalBoard*(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< StimParameters*(*)>(_a[4]))); break;
        case 103: _t->setAnalogOutSequenceParameters((*reinterpret_cast< Rhs2000EvalBoard*(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3])),(*reinterpret_cast< StimParameters*(*)>(_a[4]))); break;
        case 104: _t->setStimSequenceParameters((*reinterpret_cast< Rhs2000EvalBoard*(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2])),(*reinterpret_cast< double(*)>(_a[3])),(*reinterpret_cast< int(*)>(_a[4])),(*reinterpret_cast< int(*)>(_a[5])),(*reinterpret_cast< StimParameters*(*)>(_a[6]))); break;
        case 105: _t->ampSettleSettings(); break;
        case 106: _t->chargeRecoverySettings(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 0:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QWidget* >(); break;
            }
            break;
        case 1:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QWidget* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (MainWindow::*_t)(QWidget * , QWidget * );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::focusChanged)) {
                *result = 0;
                return;
            }
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 107)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 107;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 107)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 107;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::focusChanged(QWidget * _t1, QWidget * _t2)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
